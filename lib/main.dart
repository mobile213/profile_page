import 'dart:html';

import 'package:flutter/material.dart';

void main() {
  runApp(ContactProfilePage());
}

class ContactProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.white10,
          // iconTheme: IconThemeData(
          //   color: Colors.purple,
          // ),
        ),
        iconTheme: IconThemeData(
          color: Colors.purple
        )
      ),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }

  AppBar buildAppBarWidget(){
    return AppBar(
        backgroundColor: Colors.blueGrey,
        leading: Icon(
          Icons.arrow_back,
          color: Colors.black,
        ),
        // title: Text("Hello Flutter App"),
        actions: <Widget>[
          IconButton(
              onPressed: (){},
              icon: Icon(
                Icons.star_border_outlined,
                color: Colors.black,
              )
          ),
        ],
      );
  }
  Widget buildBodyWidget(){
    return ListView (
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              // color: Colors.cyan,
              width: double.infinity,
              //Height constraint at Container widget level
              height: 250,
              child: Image.network(
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKrKBDo7RjIJkrLRM8bRjh2G74fvvlRQdX5g&usqp=CAU",
                fit: BoxFit.cover,
              ),
            ),
            Container(
              height: 60,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(8.0),
                    child: Text("PCY TNPS",
                      style: TextStyle(fontSize: 30),
                    ),
                  )
                ],
              ),
            ),
            Divider(
              color: Colors.grey,
            ),
            Container(
              margin: EdgeInsets.only(top: 8, bottom: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  buildCallButton(),
                  buildTextButton(),
                  buildVideoButton(),
                  buildEmailButton(),
                  buildDirectionsButton(),
                  buildPayButton(),

                  // mobilePhoneListTile(),
                ],
              ),
            ),
            Divider(
              color: Colors.grey,
            ),
            mobilePhoneListTile(),
            otherPhoneListTile(),
            emailListTile(),
            addressListTile(),

          ],
        ),
      ],
    );
  }

  Widget buildCallButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.call,
            color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Call"),
      ],
    );
  }
  Widget buildTextButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.textsms,
            color: Colors.purple,
          ),
          onPressed: () {},
        ),
        Text("Message"),
      ],
    );
  }
  Widget buildVideoButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.video_call,
            color: Colors.purple,
          ),
          onPressed: () {},
        ),
        Text("Video"),
      ],
    );
  }
  Widget buildEmailButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.email,
            color: Colors.purple,
          ),
          onPressed: () {},
        ),
        Text("Email"),
      ],
    );
  }
  Widget buildDirectionsButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.directions,
            color: Colors.purple,
          ),
          onPressed: () {},
        ),
        Text("Directions"),
      ],
    );
  }
  Widget buildPayButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.payments,
            color: Colors.purple,
          ),
          onPressed: () {},
        ),
        Text("Pay"),
      ],
    );
  }
  //ListTile
  Widget mobilePhoneListTile(){
    return ListTile(
      leading: Icon(Icons.call),
      title: Text("111-555-5555"),
      subtitle: Text("mobile"),
      trailing: IconButton(
        icon: Icon(Icons.message),
        color: Colors.purple,
        onPressed: () {},
      ),
    );
  }
  Widget otherPhoneListTile(){
    return ListTile(
      leading: Icon(Icons.call),
      title: Text("324-415-7678"),
      subtitle: Text("other"),
      trailing: IconButton(
        icon: Icon(Icons.message),
        color: Colors.purple,
        onPressed: () {},
      ),
    );
  }
  Widget emailListTile(){
    return ListTile(
      leading: Icon(Icons.email),
      title: Text("pcy.tnps@gmail.com"),
      subtitle: Text("Work"),

    );
  }
  Widget addressListTile(){
    return ListTile(
      leading: Icon(Icons.location_history),
      title: Text("99 trat 23120"),
      subtitle: Text("home"),
      trailing: IconButton(
        icon: Icon(Icons.directions),
        color: Colors.purple,
        onPressed: () {},
      ),
    );
  }
}